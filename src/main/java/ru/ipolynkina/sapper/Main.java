package ru.ipolynkina.sapper;

import ru.ipolynkina.sapper.frame.MainFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(MainFrame::new);
    }
}
