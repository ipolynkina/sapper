package ru.ipolynkina.sapper.board;

import ru.ipolynkina.sapper.generator.Generator;

public interface Board {

    void setSize(int x, int y);

    void setAmountBomb(int amountBomb);

    void setGenerator(Generator generator);

    void generateBoard();

    boolean isBomb(int x, int y);

    void setGuessBomb(int x, int y);

    void openCell(int x, int y);

    boolean allCellsIsOpen();

    void openAllBomb();
}
