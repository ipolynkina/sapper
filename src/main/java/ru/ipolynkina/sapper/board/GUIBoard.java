package ru.ipolynkina.sapper.board;

import ru.ipolynkina.sapper.cell.Cell;
import ru.ipolynkina.sapper.cell.GUICell;
import ru.ipolynkina.sapper.generator.Generator;
import ru.ipolynkina.sapper.setting.GUISetting;

import javax.swing.*;
import java.awt.*;

public class GUIBoard extends JPanel implements Board {

    private static final int PIXELS_IN_PADDING = GUISetting.getGUIPadding();

    private int sizeX;
    private int sizeY;
    private int amountBomb;
    private GUICell[][] cells;
    private Generator generator;

    public GUIBoard(){}

    @Override
    public void setSize(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        cells = new GUICell[sizeX][sizeY];
        for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {
                cells[x][y] = new GUICell();
            }
        }
    }

    @Override
    public void setAmountBomb(int amountBomb) {
        this.amountBomb = amountBomb;
    }

    @Override
    public void setGenerator(Generator generator) {
        this.generator = generator;
    }

    @Override
    public void generateBoard() {
        for(Cell[] row : cells) {
            for(Cell cell : row) {
                cell.setValue(0);
            }
        }

        cells = (GUICell[][]) generator.generateBoard(cells, sizeX, sizeY, amountBomb);
    }

    @Override
    public boolean isBomb(int x, int y) {
        return cells[x][y].isBomb();
    }

    @Override
    public void setGuessBomb(int x, int y) {
        cells[x][y].setGuessBomb();
    }

    @Override
    public void openCell(int x, int y) {
        cells[x][y].openCell();
        if(cells[x][y].getValue() == 0) {
            openNullCellsNearby(x, y);
        }
    }

    @Override
    public void openAllBomb() {
        for(Cell[] row : cells) {
            for(Cell cell : row) {
                if(cell.isBomb()) cell.openCell();
            }
        }
    }

    @Override
    public boolean allCellsIsOpen() {
        boolean isAllCellsOpen = true;

        for(Cell[] row : cells) {
            for(Cell cell : row) {
                if(!cell.isBomb() && !cell.isOpen()) {
                    isAllCellsOpen = false;
                    break;
                }
            }
        }

        return isAllCellsOpen;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {
                graphics.setColor(Color.BLACK);
                cells[x][y].draw(graphics, x, y);
                graphics.drawRect(x * PIXELS_IN_PADDING, y * PIXELS_IN_PADDING, PIXELS_IN_PADDING, PIXELS_IN_PADDING);
            }
        }
    }

    private void openNullCellsNearby(int x, int y) {
        int top = x - 1 >= 0 ? x - 1 : x;
        int down = x + 1 < sizeX ? x + 1 : x;
        int left = y - 1 >= 0 ? y - 1 : y;
        int right = y + 1 < sizeY ? y + 1 : y;

        for(int i = top; i <= down; i++) {
            for(int j = left; j <= right; j++) {
                if(i == x && j == y) continue;
                if(cells[i][j].isOpen()) continue;

                cells[i][j].openCell();
                if(cells[i][j].getValue() == 0) openNullCellsNearby(i, j);
            }
        }
    }
}
