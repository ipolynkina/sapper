package ru.ipolynkina.sapper.cell;

public interface Cell {

    int BOMB_DESIGNATION = -1;
    int MAX_CELL_VALUE = 9;

    void setValue(int value);

    int getValue();

    void setBomb();

    boolean isBomb();

    void setGuessBomb();

    void openCell();

    boolean isOpen();
}
