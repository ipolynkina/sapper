package ru.ipolynkina.sapper.cell;

import ru.ipolynkina.sapper.setting.GUISetting;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class GUICell implements Cell, Painter<Graphics> {

    private static final int PIXELS_IN_PADDING = GUISetting.getGUIPadding();

    private static BufferedImage imgBomb;
    private static BufferedImage imgGuessBomb;
    private static BufferedImage imgClose;
    private static BufferedImage[] imgNum = new BufferedImage[Cell.MAX_CELL_VALUE];

    private int value;
    private boolean isBomb;
    private boolean isOpen;
    private boolean isGuessBomb;

    static {
        try {
            imgBomb = ImageIO.read(Objects.requireNonNull(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("img/bomb.jpg")));

            imgGuessBomb = ImageIO.read(Objects.requireNonNull(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("img/flag.jpg")));

            imgClose = ImageIO.read(Objects.requireNonNull(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("img/close.jpg")));

            for(int i = 0; i < imgNum.length; i++) {
                imgNum[i] = ImageIO.read(Objects.requireNonNull(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("img/" + i + ".jpg")));
            }
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    public GUICell() {
        value = 0;
        isBomb = false;
        isOpen = false;
        isGuessBomb = false;
    }

    @Override
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public void setBomb() {
        isBomb = true;
    }

    @Override
    public boolean isBomb() {
        return isBomb;
    }

    @Override
    public void setGuessBomb() {
        this.isGuessBomb = true;
    }

    @Override
    public void openCell() {
        isOpen = true;
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void draw(Graphics paint, int x, int y) {
        if(!isOpen) {
            paint.drawImage(imgClose, x * PIXELS_IN_PADDING, y * PIXELS_IN_PADDING, null);
        }

        if(!isOpen && isGuessBomb) {
            paint.drawImage(imgGuessBomb, x * PIXELS_IN_PADDING, y * PIXELS_IN_PADDING, null);
        }

        if(isOpen) {
            if(isBomb) {
                paint.drawImage(imgBomb, x * PIXELS_IN_PADDING, y * PIXELS_IN_PADDING, null);
            } else {
                paint.drawImage(imgNum[value], x * PIXELS_IN_PADDING, y * PIXELS_IN_PADDING, null);
            }
        }
    }
}
