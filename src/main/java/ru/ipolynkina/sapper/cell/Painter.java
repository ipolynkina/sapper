package ru.ipolynkina.sapper.cell;

/*
* позволяет работать
* с различными типами данных
* без изменения их описания
* */
public interface Painter<T> {

    void draw(T paint, int x, int y);
}
