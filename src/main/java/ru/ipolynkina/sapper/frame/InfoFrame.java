package ru.ipolynkina.sapper.frame;

import ru.ipolynkina.sapper.setting.GUISetting;

import javax.swing.*;
import java.awt.*;

public class InfoFrame extends JDialog {

    public InfoFrame(JFrame owner, String text) {
        super(owner, "Sapper", true);
        setSize(GUISetting.getInfoFrameWidth(), GUISetting.getInfoFrameHeight());
        setLocationRelativeTo(owner);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        createTextPanel(text);
        createButtonPanel();
    }

    private void createTextPanel(String text) {
        JPanel textPanel = new JPanel();

        JLabel label = new JLabel();
        label.setText("<html><br>" + text + "<br></html>");
        label.setForeground(Color.BLUE);
        textPanel.add(label, BorderLayout.NORTH);

        add(textPanel, BorderLayout.NORTH);
    }

    private void createButtonPanel() {
        JPanel buttonPanel = new JPanel();

        JButton btnOk = new JButton("OK");
        btnOk.addActionListener(e -> dispose());
        buttonPanel.add(btnOk, BorderLayout.PAGE_END);

        add(buttonPanel, BorderLayout.SOUTH);
    }
}
