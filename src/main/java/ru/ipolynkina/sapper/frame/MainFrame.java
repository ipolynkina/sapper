package ru.ipolynkina.sapper.frame;

import ru.ipolynkina.sapper.board.GUIBoard;
import ru.ipolynkina.sapper.gamer.Gamer;
import ru.ipolynkina.sapper.generator.StandardGenerator;
import ru.ipolynkina.sapper.logic.GUILogic;
import ru.ipolynkina.sapper.setting.GUISetting;
import ru.ipolynkina.sapper.setting.Setting;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MainFrame extends JFrame {

    private GUIBoard board;

    public MainFrame() {
        setTitle("Sapper");

        Dimension dimension = new Dimension();
        dimension.width = calculateWidthFrame();
        dimension.height = calculateHeightFrame();
        setPreferredSize(dimension);
        pack();

        setResizable(false);
        setLocationRelativeTo(null);

        createEmptyBoard();
        createButtonPanel();
        setVisible(true);
    }

    private void createEmptyBoard() {
        board = new GUIBoard();
        int defaultSize = 10;
        board.setBorder(new EmptyBorder(defaultSize, defaultSize, defaultSize, defaultSize));
        add(board, BorderLayout.CENTER);
    }

    private void createButtonPanel() {
        JPanel buttonPanel = new JPanel();

        JButton btnStartGame = new JButton("Новая Игра");
        btnStartGame.addActionListener(new GUILogic(board, new StandardGenerator(), new Gamer(), this));

        JButton btnSetting = new JButton("Настройки");
        btnSetting.addActionListener(event -> {
            SettingFrame settingFrame = new SettingFrame(this);
            settingFrame.setVisible(true);
            setSize(calculateWidthFrame(), calculateHeightFrame());
            btnStartGame.doClick();
        });

        buttonPanel.add(btnSetting);
        buttonPanel.add(btnStartGame);
        add(buttonPanel, BorderLayout.PAGE_END);
    }

    private int calculateWidthFrame() {
        return Setting.getCurrentSizeX() * (GUISetting.getGUIPadding() + 1);
    }

    private int calculateHeightFrame() {
        return Setting.getCurrentSizeY() * GUISetting.getGUIPadding() + GUISetting.getMainFramePanelHeight();
    }
}
