package ru.ipolynkina.sapper.frame;

import ru.ipolynkina.sapper.setting.GUISetting;
import ru.ipolynkina.sapper.setting.Setting;

import javax.swing.*;
import java.awt.*;

import static ru.ipolynkina.sapper.setting.Setting.*;

public class SettingFrame extends JDialog {

    private JComboBox<String> fieldProperties;
    private JComboBox<String> errorProperties;

    public SettingFrame(JFrame owner) {
        super(owner, "Setting", true);
        setSize(GUISetting.getSettingFrameWidth(), GUISetting.getSettingFrameHeight());
        setLocationRelativeTo(owner);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        createFieldPanel();
        createErrorPanel();
        createActionPanel();
    }

    private void createFieldPanel() {
        JPanel fieldPanel = new JPanel();

        JLabel fieldText = new JLabel("Сложность ");
        fieldPanel.add(fieldText);

        String[] description = new String[Setting.getFieldProperties().size()];
        for(int i = 0; i < description.length; i++) {
            description[i] = Setting.getFieldByIndex(i).toString();
        }
        fieldProperties = new JComboBox<>(description);
        fieldProperties.setSelectedIndex(Setting.getCurrentFieldIndex());
        fieldPanel.add(fieldProperties);

        add(fieldPanel, BorderLayout.PAGE_START);
    }

    private void createErrorPanel() {
        JPanel errorPanel = new JPanel();

        JLabel errorText = new JLabel("Допустимые ошибки");
        errorPanel.add(errorText);

        String[] description = new String[Setting.getErrorProperties().size()];
        for(int i = 0; i < description.length; i++) {
            description[i] = Setting.getErrorByIndex(i).toString();
        }
        errorProperties = new JComboBox<>(description);
        errorProperties.setSelectedIndex(Setting.getCurrentErrorIndex());
        errorPanel.add(errorProperties);

        add(errorPanel, BorderLayout.CENTER);
    }

    private void createActionPanel() {
        JPanel actionPanel = new JPanel();

        JButton btnOk = new JButton("Сохранить");
        btnOk.addActionListener(event -> {
            setCurrentFieldIndex(fieldProperties.getSelectedIndex());
            setCurrentErrorIndex(errorProperties.getSelectedIndex());
            dispose();
        });

        JButton btnCancel = new JButton("Отмена");
        btnCancel.addActionListener(event -> dispose());
        actionPanel.add(btnOk);
        actionPanel.add(btnCancel);

        add(actionPanel, BorderLayout.PAGE_END);
    }
}
