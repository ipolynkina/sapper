package ru.ipolynkina.sapper.gamer;

public class Gamer {

    private Status status;

    public Gamer() {
        status = Status.UNKNOWN;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
