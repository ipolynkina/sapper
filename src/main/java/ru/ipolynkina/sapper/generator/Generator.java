package ru.ipolynkina.sapper.generator;

import ru.ipolynkina.sapper.cell.Cell;

public interface Generator {

    Cell[][] generateBoard(Cell[][] cells, int x, int y, int amountBomb);
}
