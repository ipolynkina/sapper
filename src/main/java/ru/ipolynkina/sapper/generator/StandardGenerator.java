package ru.ipolynkina.sapper.generator;

import ru.ipolynkina.sapper.cell.Cell;

public class StandardGenerator implements Generator {

    private Cell[][] cells;

    @Override
    public Cell[][] generateBoard(Cell[][] cells, int sizeX, int sizeY, int amountBomb) {
        this.cells = cells;

        generateBomb(sizeX, sizeY, amountBomb);
        fillCellsValue(sizeX, sizeY);

        return cells;
    }

    private void generateBomb(int sizeX, int sizeY, int amountBomb) {
        int haveBomb = 0;
        while(haveBomb != amountBomb) {
            int x = (int) (Math.random() * sizeX);
            int y = (int) (Math.random() * sizeY);
            if(!cells[x][y].isBomb()) {
                cells[x][y].setValue(Cell.BOMB_DESIGNATION);
                cells[x][y].setBomb();
                haveBomb++;
            }
        }
    }

    private void fillCellsValue(int sizeX, int sizeY) {
        for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {
                if(cells[x][y].getValue() != Cell.BOMB_DESIGNATION) {
                    cells[x][y].setValue(calculateValue(x, y, sizeX, sizeY));
                }
            }
        }
    }

    private int calculateValue(int x, int y, int sizeX, int sizeY) {
        int top = x - 1 >= 0 ? x - 1 : x;
        int down = x + 1 < sizeX ? x + 1 : x;
        int left = y - 1 >= 0 ? y - 1 : y;
        int right = y + 1 < sizeY ? y + 1 : y;

        int amountBomb = 0;
        for(int i = top; i <= down; i++) {
            for(int j = left; j <= right; j++) {
                if(i == x && j == y) continue;
                if(cells[i][j].isBomb()) amountBomb++;

            }
        }

        return amountBomb;
    }
}
