package ru.ipolynkina.sapper.logic;

import ru.ipolynkina.sapper.frame.InfoFrame;
import ru.ipolynkina.sapper.board.GUIBoard;
import ru.ipolynkina.sapper.gamer.Gamer;
import ru.ipolynkina.sapper.gamer.Status;
import ru.ipolynkina.sapper.generator.Generator;
import ru.ipolynkina.sapper.setting.GUISetting;
import ru.ipolynkina.sapper.setting.Setting;

import javax.swing.*;
import java.awt.event.*;

public class GUILogic extends StandardLogic implements ActionListener, MouseListener {

    private static final int PADDING = GUISetting.getGUIPadding();

    private int x;
    private int y;
    private boolean isGuessBomb;

    private GUIBoard board;
    private JFrame owner;
    private int haveError;

    public GUILogic(GUIBoard board, Generator generator, Gamer gamer, JFrame owner) {
        super(board, generator, gamer);
        this.board = board;
        this.board.addMouseListener(this);
        this.owner = owner;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        haveError = 0;
        startGame();
        board.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        saveCoordinates(event);
        openCell(x, y, isGuessBomb);
        board.repaint();
    }

    @Override
    public void mousePressed(MouseEvent event) {
        saveCoordinates(event);
        openCell(x, y, isGuessBomb);
        board.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        saveCoordinates(event);
        openCell(x, y, isGuessBomb);
        board.repaint();
    }

    private void saveCoordinates(MouseEvent event) {
        x = event.getX() / PADDING;
        y = event.getY() / PADDING;
        isGuessBomb = event.getButton() == MouseEvent.BUTTON3;
    }

    @Override
    protected void setLoserStatus() {
        haveError++;
        if(haveError > Setting.getCurrentAmountError()) {
            super.setLoserStatus();
        } else showInfo("Вы допустили " + haveError + " ошибку");
    }

    @Override
    protected void printMessage() {
        if(getGamerStatus() == Status.LOSER) {
            showInfo("Неудача! Попробуйте сыграть еще раз!");
        } else if(getGamerStatus() == Status.WINNER) {
            showInfo("Поздравляем! Вы победили!");
        }
    }

    private void showInfo(String text) {
        InfoFrame infoFrame = new InfoFrame(owner, text);
        infoFrame.setVisible(true);
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
