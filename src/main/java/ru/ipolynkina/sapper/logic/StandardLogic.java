package ru.ipolynkina.sapper.logic;

import ru.ipolynkina.sapper.board.Board;
import ru.ipolynkina.sapper.gamer.Gamer;
import ru.ipolynkina.sapper.gamer.Status;
import ru.ipolynkina.sapper.generator.Generator;
import ru.ipolynkina.sapper.setting.Setting;

public class StandardLogic {

    private Board board;
    private Generator generator;
    private Gamer gamer;

    public StandardLogic(Board board, Generator generator, Gamer gamer) {
        this.board = board;
        this.generator = generator;
        this.gamer = gamer;
    }

    public void startGame() {
        setSize();
        setAmountBomb();
        generateBoard();
        setDefaultStatus();
    }

    public void openCell(int x, int y, boolean isGuessBomb) {
        if(getGamerStatus() == Status.UNKNOWN) {
            if(isGuessBomb) {
                setGuessBomb(x, y);
            } else {
                openCell(x, y);
                if(isBomb(x, y)) setLoserStatus();
                if(isFinish()) setWinnerStatus();
            }
        }

        if(getGamerStatus() == Status.LOSER) {
            openAllBomb();
            printMessage();
        }

        if(getGamerStatus() == Status.WINNER) {
            printMessage();
        }
    }

    protected void setWinnerStatus() {
        gamer.setStatus(Status.WINNER);
    }

    protected void setDefaultStatus() {
        gamer.setStatus(Status.UNKNOWN);
    }

    protected void setLoserStatus() {
        gamer.setStatus(Status.LOSER);
    }

    protected void printMessage() {
        if(gamer.getStatus() == Status.LOSER) {
            System.out.println("Неудача! Попробуйте сыграть еще раз!");
        } else if(gamer.getStatus() == Status.WINNER) {
            System.out.println("Поздравляем! Вы победили!");
        }
    }

    protected void setSize() {
        int sizeX = Setting.getCurrentSizeX();
        int sizeY = Setting.getCurrentSizeY();
        board.setSize(sizeX, sizeY);
    }

    protected void setAmountBomb() {
        int amountBomb = Setting.getCurrentAmountBomb();
        board.setAmountBomb(amountBomb);
    }

    private void generateBoard() {
        board.setGenerator(generator);
        board.generateBoard();
    }

    public Status getGamerStatus() {
        return gamer.getStatus();
    }

    private void setGuessBomb(int x, int y) {
        board.setGuessBomb(x, y);
    }

    private void openCell(int x, int y) {
        board.openCell(x, y);
    }

    private boolean isBomb(int x, int y) {
        return board.isBomb(x, y);
    }

    private void openAllBomb() {
        board.openAllBomb();
    }

    private boolean isFinish() {
        return board.allCellsIsOpen();
    }
}
