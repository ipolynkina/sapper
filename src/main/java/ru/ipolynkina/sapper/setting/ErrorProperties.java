package ru.ipolynkina.sapper.setting;

public class ErrorProperties {

    private int amountError;

    public ErrorProperties(int amountError) {
        this.amountError = amountError;
    }

    public int getAmountError() {
        return amountError;
    }

    public void setAmountError(int amountError) {
        this.amountError = amountError;
    }

    @Override
    public String toString() {
        return String.valueOf(amountError);
    }
}
