package ru.ipolynkina.sapper.setting;

public class FieldProperties {

    private int sizeX;
    private int sizeY;
    private int amountBomb;

    public FieldProperties(int sizeX, int sizeY, int amountBomb) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.amountBomb = amountBomb;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public int getAmountBomb() {
        return amountBomb;
    }

    public void setAmountBomb(int amountBomb) {
        this.amountBomb = amountBomb;
    }

    @Override
    public String toString() {
        return "поле: " + sizeX + "x" + sizeY + ", бомбы: " + amountBomb;
    }
}
