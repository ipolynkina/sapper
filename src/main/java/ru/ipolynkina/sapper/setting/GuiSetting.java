package ru.ipolynkina.sapper.setting;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GUISetting {

    private static int guiPadding = 0;

    private static int infoFrameWidth = 0;
    private static int infoFrameHeight = 0;

    private static int settingFrameWidth = 0;
    private static int settingFrameHeight = 0;

    private static int mainFramePanelHeight = 0;

    static {
        Properties property = new Properties();
        try(InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("gui.properties")) {
            property.load(is);

            guiPadding = Integer.parseInt(property.getProperty("gui.padding"));

            infoFrameWidth = Integer.parseInt(property.getProperty("gui.infoFrame.width"));
            infoFrameHeight = Integer.parseInt(property.getProperty("gui.infoFrame.height"));

            settingFrameWidth = Integer.parseInt(property.getProperty("gui.settingFrame.width"));
            settingFrameHeight = Integer.parseInt(property.getProperty("gui.settingFrame.height"));

            mainFramePanelHeight = Integer.parseInt(property.getProperty("gui.mainFrame.bottomPanelHeight"));

        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    public static int getGUIPadding() {
        return guiPadding;
    }

    public static int getInfoFrameWidth() {
        return infoFrameWidth;
    }

    public static int getInfoFrameHeight() {
        return infoFrameHeight;
    }

    public static int getSettingFrameWidth() {
        return settingFrameWidth;
    }

    public static int getSettingFrameHeight() {
        return settingFrameHeight;
    }

    public static int getMainFramePanelHeight() {
        return mainFramePanelHeight;
    }
}
