package ru.ipolynkina.sapper.setting;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

public class Setting {

    private static ArrayList<FieldProperties> fieldProperties = new ArrayList<>();
    private static ArrayList<ErrorProperties> errorProperties = new ArrayList<>();

    private static int currentFieldIndex = 0;
    private static int currentErrorIndex = 0;

    static {
        Properties property = new Properties();
        try(InputStream is = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("setting.properties")) {

            property.load(is);

            int amountOfFields = Integer.parseInt(property.getProperty("amountOfFields"));
            for(int i = 0; i < amountOfFields; i++) {
                int sizeX = Integer.parseInt(property.getProperty("field" + i + ".sizeX"));
                int sizeY = Integer.parseInt(property.getProperty("field" + i + ".sizeY"));
                int amountBomb = Integer.parseInt(property.getProperty("field" + i + ".amountBomb"));
                fieldProperties.add(new FieldProperties(sizeX, sizeY, amountBomb));
            }

            int amountOfErrors = Integer.parseInt(property.getProperty("amountOfErrors"));
            for(int i = 0; i < amountOfErrors; i++) {
                int errors = Integer.parseInt(property.getProperty("error" + i));
                Setting.errorProperties.add(new ErrorProperties(errors));
            }

        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    public static ArrayList<FieldProperties> getFieldProperties() {
        return fieldProperties;
    }

    public static ArrayList<ErrorProperties> getErrorProperties() {
        return errorProperties;
    }

    public static int getCurrentFieldIndex() {
        return currentFieldIndex;
    }

    public static void setCurrentFieldIndex(int index) {
        currentFieldIndex = index;
    }

    public static int getCurrentErrorIndex() {
        return currentErrorIndex;
    }

    public static void setCurrentErrorIndex(int index) {
        currentErrorIndex = index;
    }

    public static FieldProperties getFieldByIndex(int index) {
        if(index >= 0 && index < fieldProperties.size()) {
            return fieldProperties.get(index);
        }
        return new FieldProperties(0, 0, 0);
    }

    public static ErrorProperties getErrorByIndex(int index) {
        if(index >= 0 && index < errorProperties.size()) {
            return errorProperties.get(index);
        }
        return new ErrorProperties(0);
    }

    public static int getCurrentSizeX() {
        return fieldProperties.get(currentFieldIndex).getSizeX();
    }

    public static int getCurrentSizeY() {
        return fieldProperties.get(currentFieldIndex).getSizeY();
    }

    public static int getCurrentAmountBomb() {
        return fieldProperties.get(currentFieldIndex).getAmountBomb();
    }

    public static int getCurrentAmountError() {
        return errorProperties.get(currentErrorIndex).getAmountError();
    }
}
