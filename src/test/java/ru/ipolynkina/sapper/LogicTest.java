package ru.ipolynkina.sapper;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.ipolynkina.sapper.board.Board;
import ru.ipolynkina.sapper.gamer.Gamer;
import ru.ipolynkina.sapper.gamer.Status;

public class LogicTest {

    private static Gamer gamer;
    private static Board board;
    private static SimpleLogic logic;

    @BeforeClass
    public static void init() {
        gamer = new Gamer();
        board = new SimpleBoard();
        logic = new SimpleLogic(board, new SimpleGenerator(), gamer);
    }

    @Test
    public void loserStatusTest() {
        logic.startGame();
        logic.openCell(0, 0, false);
        logic.openCell(0, 1, false);
        logic.openCell(1, 1, false);
        Assert.assertTrue(gamer.getStatus() == Status.LOSER);
    }

    @Test
    public void winnerStatusTest() {
        logic.startGame();
        logic.openCell(0, 0, false);
        logic.openCell(0, 1, false);
        logic.openCell(1, 0, false);
        Assert.assertTrue(gamer.getStatus() == Status.WINNER);
    }

    @Test
    public void unknownStatusTest() {
        logic.startGame();
        logic.openCell(0, 0, false);
        logic.openCell(0, 1, false);
        Assert.assertTrue(gamer.getStatus() == Status.UNKNOWN);
    }

}
