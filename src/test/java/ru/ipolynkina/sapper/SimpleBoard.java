package ru.ipolynkina.sapper;

import ru.ipolynkina.sapper.board.Board;
import ru.ipolynkina.sapper.generator.Generator;

public class SimpleBoard implements Board {

    static final int TEST_SIZE_X = 2;
    static final int TEST_SIZE_Y = 2;
    static final int TEST_AMOUNT_BOMB = 1;

    private int amountBomb;
    private SimpleCell[][] cells;
    private Generator generator;

    @Override
    public void setSize(int x, int y) {
        cells = new SimpleCell[TEST_SIZE_X][TEST_SIZE_Y];
        for(int i = 0; i < TEST_SIZE_X; i++) {
            for(int j = 0; j < TEST_SIZE_Y; j++) {
                cells[i][j] = new SimpleCell();
            }
        }
    }

    @Override
    public void setAmountBomb(int amountBomb) {
        this.amountBomb = TEST_AMOUNT_BOMB;
    }

    @Override
    public void setGenerator(Generator generator) {
        this.generator = generator;
    }

    @Override
    public void generateBoard() {
        generator.generateBoard(cells, TEST_SIZE_X, TEST_SIZE_Y, TEST_AMOUNT_BOMB);
    }

    @Override
    public boolean isBomb(int x, int y) {
        return cells[x][y].isBomb();
    }

    @Override
    public void setGuessBomb(int x, int y) {
        cells[x][y].setGuessBomb();
    }

    @Override
    public void openCell(int x, int y) {
        cells[x][y].openCell();
    }

    @Override
    public boolean allCellsIsOpen() {
        return cells[0][0].isOpen() && cells[0][1].isOpen() && cells[1][0].isOpen();
    }

    @Override
    public void openAllBomb() {
        cells[1][1].openCell();
    }
}
