package ru.ipolynkina.sapper;

import ru.ipolynkina.sapper.cell.Cell;
import ru.ipolynkina.sapper.cell.Painter;

public class SimpleCell implements Cell, Painter {

    private int value;
    private boolean isBomb;
    private boolean isOpen;
    private boolean isGuessBomb;

    public SimpleCell() {
        value = 0;
        isBomb = false;
        isOpen = false;
        isGuessBomb = false;
    }

    @Override
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public void setBomb() {
        isBomb = true;
    }

    @Override
    public boolean isBomb() {
        return isBomb;
    }

    @Override
    public void setGuessBomb() {
        this.isGuessBomb = true;
    }

    @Override
    public void openCell() {
        isOpen = true;
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void draw(Object paint, int x, int y) {
        System.out.print(" " + value + " ");
    }
}
