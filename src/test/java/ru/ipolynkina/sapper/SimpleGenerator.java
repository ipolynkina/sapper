package ru.ipolynkina.sapper;

import ru.ipolynkina.sapper.cell.Cell;
import ru.ipolynkina.sapper.generator.StandardGenerator;

public class SimpleGenerator extends StandardGenerator {

    @Override
    public Cell[][] generateBoard(Cell[][] cells, int x, int y, int amountBomb) {
        cells[0][0].setValue(1);
        cells[0][1].setValue(1);
        cells[1][0].setValue(1);
        cells[1][1].setValue(-1);
        cells[1][1].setBomb();
        return cells;
    }
}
