package ru.ipolynkina.sapper;

import ru.ipolynkina.sapper.board.Board;
import ru.ipolynkina.sapper.gamer.Gamer;
import ru.ipolynkina.sapper.generator.Generator;
import ru.ipolynkina.sapper.logic.StandardLogic;

public class SimpleLogic extends StandardLogic {

    private Board board;
    private Generator generator;

    public SimpleLogic (Board board, Generator generator, Gamer gamer) {
        super(board, generator, gamer);
        this.board = board;
        this.generator = generator;
    }

    @Override
    protected void setSize() {
        int sizeX = SimpleBoard.TEST_SIZE_X;
        int sizeY = SimpleBoard.TEST_SIZE_Y;
        board.setSize(sizeX, sizeY);
    }

    @Override
    protected void setAmountBomb() {
        int amountBomb = SimpleBoard.TEST_AMOUNT_BOMB;
        board.setAmountBomb(amountBomb);
    }
}
